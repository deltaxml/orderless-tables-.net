@echo off
..\..\bin\deltaxml-docbook.exe compare  ^
                docbook-orderless-table-version-1-no-PI.xml ^
                docbook-orderless-table-version-2-no-PI.xml ^
                result-no-PI.xml indent=yes

..\..\bin\deltaxml-docbook.exe compare  ^
                docbook-orderless-table-version-1-with-PI.xml ^
                docbook-orderless-table-version-2-with-PI.xml ^
                result-with-PI.xml indent=yes

..\..\bin\deltaxml-docbook.exe compare  ^
                docbook-orderless-table-version-1-with-PI-and-key.xml ^
                docbook-orderless-table-version-2-with-PI-and-key.xml ^
                result-with-PI-and-key.xml indent=yes